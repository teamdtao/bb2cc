# Bitbucket to Confluence Cloud

This project is based on [bb2confluence](https://bitbucket.org/dtao/bb2confluence);
but where that project aspired to be an app, this is simply a library that lets you
sync a file from a Bitbucket repository to a page in Confluence Cloud.

## Usage

To sync all of the files changed in a commit:

```
bb2cc -u <username> -p <password> -r <repository> --commit <commit>
```

To sync all of the files at a given path:

```
bb2cc -u <username> -p <password> -r <repository> --path <path>
```

## Format

In order to sync a document to Confluence, it should have the extension `.md`
or `.markdown` and must contain at least the following frontmatter at the top
of the document:

```
---
page_id: <Confluence page ID>
title: <Confluence page title>
---
```

### Additional options

In addition to the required `page_id` and `title` frontmatter properties, the
following optional properties are supported.

```
header: false
```

Render a header at the top of the page informing the reader that it's kept in
sync with a Bitbucket repository. True by default.

```
toc: true
```

Render a table of contents at the top of the page (below the header). False by
default.

## Confluence features

In addition to [standard Markdown syntax][1], this library has built-in support
for certain Confluence features.

### Autolinks

Links to other Confluence pages are automatically rendered as inline cards.

### User mentions

Mention a user by putting a `@` before their nickname:

```
Meeting with @dtao this morning to discuss those TPS reports.
```

### Tables

Tables are supported using [extended Markdown syntax][2]:

```
| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |
```

### Panels

The syntax for rendering an info panel is the same as for rendering a [fenced
code block][3], but with `panel:<type>` as the "language" for the block:

    ```panel:info
    This will be rendered as an info panel.
    ```

### Status lozenges

To render a status lozenge, surround the text with `[[` and `]]`:

```
Status: [[On track]]

You can also provide a color: [[At risk:Red]]
```

### Task lists

To create a task list:

```
- [x] Completed task
- [ ] Incomplete task
```

## Development

To run the tests locally:

```
pytest
```

[1]: https://www.markdownguide.org/basic-syntax
[2]: https://www.markdownguide.org/extended-syntax/#tables
[3]: https://www.markdownguide.org/extended-syntax/#fenced-code-blocks
