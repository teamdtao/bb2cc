"""Test cases for the md2cc module."""

from textwrap import dedent

from cc import Confluence
from md2cc import render_to_html


def test_autolink():
    """Test that links to Confluence pages are rendered as inline cards."""
    mock_client = Confluence(host='https://company.atlassian.net/wiki/',
                             username='foo', password='bar')
    mock_client.get_page = lambda page_id: {'title': 'fake title'}

    html = render_to_html(
        'https://company.atlassian.net/wiki/pages/1337', mock_client)

    assert html == (
        '<p><ac:link ac:card-appearance="inline">'
        '<ri:page ri:content-title="fake title" />'
        '<ac:link-body>fake title</ac:link-body>'
        '</ac:link></p>'
    )


def test_info_panel():
    """Test custom syntax for rendering a panel."""
    assert render_to_html(dedent('''
        ```panel:info
        This is an info panel.
        ```
    ''')) == (
        '<ac:structured-macro ac:name="info">'
        '<ac:rich-text-body><p>This is an info panel.</p></ac:rich-text-body>'
        '</ac:structured-macro>'
    )


def test_image():
    """Test that images are rendered using the `<ac:image>` tag."""
    assert render_to_html(dedent('''
        ![Some image](https://example.com/some-image.jpg)
    ''')) == (
        '<p><ac:image ac:alt="Some image">'
        '<ri:url ri:value="https://example.com/some-image.jpg" />'
        '</ac:image></p>'
    )


def test_relative_link():
    """Test that a relative link is updated to point to the repository."""
    html = render_to_html(
        '[some link](docs/some-page.md)',
        repository_url='https://bitbucket.org/foo/bar')

    assert html == (
        '<p>'
        '<a href="https://bitbucket.org/foo/bar/src/HEAD/docs/some-page.md">'
        'some link'
        '</a>'
        '</p>'
    )


def test_task_list():
    """Test that GFM-style checklists are rendered as task lists."""
    html = render_to_html(dedent('''
        - [ ] First *item*
        - [x] Second **item**
    '''))

    assert html == (
        '<ac:task-list>'
        '<ac:task>'
        '<ac:task-status>incomplete</ac:task-status>'
        '<ac:task-body><p>First <em>item</em></p></ac:task-body>'
        '</ac:task>'
        '<ac:task>'
        '<ac:task-status>complete</ac:task-status>'
        '<ac:task-body><p>Second <strong>item</strong></p></ac:task-body>'
        '</ac:task>'
        '</ac:task-list>'
    )


def test_strip_extra_newlines():
    """Test that extra newline characters are removed from text nodes."""
    assert render_to_html(dedent('''
        This is some
        text with extra
        newlines.
    ''')) == '<p>This is some text with extra newlines.</p>'


def test_status_lozenge():
    """Test custom `[[status]]` syntax for rendering status lozenges."""
    assert render_to_html('Hello [[beautiful]], how are you?') == (
        '<p>Hello <ac:structured-macro ac:name="status">'
        '<ac:parameter ac:name="colour">Grey</ac:parameter>'
        '<ac:parameter ac:name="title">beautiful</ac:parameter>'
        '</ac:structured-macro>, how are you?</p>'
    )


def test_user_mention():
    """Test custom `@username` syntax for rendering user mentions."""
    mock_client = Confluence(host='https://company.atlassian.net/wiki/',
                             username='foo', password='bar')
    mock_client.lookup_user = lambda username: 'fake-account-id'

    assert render_to_html('Hello, @dtao', mock_client) == (
        '<p>Hello, '
        '<ac:link><ri:user ri:username="fake-account-id" /></ac:link>'
        '</p>'
    )


def test_escape_html_entities_properly():
    """Test that potentially special HTML characters are properly encoded."""
    assert render_to_html('This should take <= 2 weeks.') == (
        '<p>This should take &lt;= 2 weeks.</p>'
    )
