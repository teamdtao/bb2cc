"""Test cases for the util module."""

from textwrap import dedent

from util import parse_content


def test_parse_content_handle_horizontal_rule():
    """Test parsing Markdown content with an "extra" <hr /> element."""
    content = dedent('''
        ---
        page_id: 42
        title: Game of Trains
        ---

        # Chapter 1

        The night was sultry.

        ---

        # Chapter 2

        The night was dark and full of terrors.
    ''')

    parse_content(content)
